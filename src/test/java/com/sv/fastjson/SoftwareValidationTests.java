package com.sv.fastjson;

import static org.junit.Assert.*;

import com.alibaba.fastjson.*;
import org.apache.commons.io.IOUtils;
import org.junit.*;

import java.io.IOException;
import java.util.Objects;

public class SoftwareValidationTests {

    /**
     * Tests whether fastjson can convert a JSON string to a Java object correctly
     *
     * [E]xistence - Checks whether the parsed Java object exists (is not null), and whether its fields are set properly
     */
    @Test
    public void jsonToJava() {
        String jsonString = "{\"id\":0,\"name\":\"Justin Smid\"}";

        User parsedUser = JSON.parseObject(jsonString, User.class);

        assertNotNull(parsedUser);
        assertEquals(parsedUser.getId(), 0);
        assertEquals(parsedUser.getName(), "Justin Smid");
    }

    /**
     * Tests whether fastjson throws an exception when the user attempt to parse an invalid JSON-string
     *
     * [C]onformance - Ensures fastjson checks whether a string it attempts to parse conforms to the JSON-standards
     */
    @Test
    public void throwOnInvalidJSON() {
        String invalidJSONString = "{\"id\"=0,\"name\"=\"Justin Smid\"}";

        Exception exception = assertThrows(JSONException.class, () -> JSON.parseObject(invalidJSONString, User.class));

        // Verify that the exception message mentions the correct error, namely the fact we used `=` instead of `:`.
        assertTrue(exception.getMessage().contains("not match : - ="));
    }

    /**
     * Tests whether fastjson can convert a Java object to a JSON string correctly
     *
     * [E]xistence - Checks whether all the fields set in the Java object exist, and are equal to those in the Java object
     */
    @Test
    public void javaToJSON() {
        User javaObject = new User(1, "Matthijs Snijders");

        String parsedJSON = JSON.toJSONString(javaObject);

        assertEquals(parsedJSON, "{\"id\":1,\"name\":\"Matthijs Snijders\"}");
    }

    /**
     * Tests whether fastjson can correctly check whether a given string is valid JSON
     *
     * [C]onformance - Ensures fastjson can check whether a string conforms to the JSON-standards
     */
    @Test
    public void checkSimpleJSONValidity() {
        String simpleValidJSON = "{\"id\":2,\"name\":\"Okechuckwu Onwunli\"}";
        String simpleInvalidJSON = "{\"id\"=2,\"name\"=\"Okechuckwu Onwunli\"}";

        boolean firstIsValid = JSONValidator.from(simpleValidJSON).validate();
        boolean secondIsInvalid = !JSONValidator.from(simpleInvalidJSON).validate();

        assertTrue(firstIsValid);
        assertTrue(secondIsInvalid);
    }

    /**
     * Tests whether fastjson can correctly check whether a given string is valid JSON
     *
     * [C]onformance - Ensures fastjson can check whether a string conforms to the JSON-standards
     */
    @Test
    public void checkComplexJSONValidity() {
        String complexValidJSON = "[ { \"_id\": \"5fd8ae8c2ce894185976f804\", \"index\": 0, \"guid\": \"cd7c0c16-ff27-4c10-aec0-4a6e6fae43a8\", \"isActive\": false, \"balance\": \"$3,825.94\", \"picture\": \"http://placehold.it/32x32\", \"age\": 37, \"eyeColor\": \"blue\", \"name\": \"Bianca Hendricks\", \"gender\": \"female\", \"company\": \"EYERIS\", \"email\": \"biancahendricks@eyeris.com\", \"phone\": \"+1 (901) 460-2574\", \"address\": \"943 Vandervoort Avenue, Ezel, West Virginia, 9750\", \"about\": \"Anim culpa nulla ea deserunt eu in aliquip dolor consectetur velit ex. Cupidatat amet aliquip ut occaecat esse. Ea exercitation et anim ad incididunt irure duis consectetur commodo aliquip voluptate qui.\\r\\n\", \"registered\": \"2015-08-24T10:56:33 -02:00\", \"latitude\": -52.480007, \"longitude\": 25.211202, \"tags\": [ \"in\", \"amet\", \"cillum\", \"nulla\", \"irure\", \"ad\", \"nulla\" ], \"friends\": [ { \"id\": 0, \"name\": \"Foreman Acevedo\" }, { \"id\": 1, \"name\": \"Glenn Collier\" }, { \"id\": 2, \"name\": \"Shelia Haley\" } ], \"greeting\": \"Hello, Bianca Hendricks! You have 4 unread messages.\", \"favoriteFruit\": \"banana\" }, { \"_id\": \"5fd8ae8cff909070cda6176b\", \"index\": 1, \"guid\": \"efb3d44e-871c-488f-b6bd-137fe5cb7dd3\", \"isActive\": true, \"balance\": \"$3,653.71\", \"picture\": \"http://placehold.it/32x32\", \"age\": 28, \"eyeColor\": \"blue\", \"name\": \"Sadie Hicks\", \"gender\": \"female\", \"company\": \"HAIRPORT\", \"email\": \"sadiehicks@hairport.com\", \"phone\": \"+1 (968) 463-2928\", \"address\": \"815 Bowne Street, Malo, South Dakota, 3260\", \"about\": \"Consectetur sint ut cupidatat dolore excepteur Lorem occaecat aute aute ad eiusmod irure. Pariatur irure ad duis consectetur. Culpa aliqua duis occaecat laboris cillum consectetur occaecat esse proident sunt anim excepteur. Commodo dolor enim voluptate consectetur velit incididunt eu ea duis exercitation. Labore velit aute cillum nostrud proident laborum id ullamco exercitation nulla tempor. Pariatur quis excepteur consectetur mollit ipsum nisi culpa laborum non.\\r\\n\", \"registered\": \"2016-11-29T10:08:05 -01:00\", \"latitude\": -79.79171, \"longitude\": 139.749893, \"tags\": [ \"id\", \"est\", \"mollit\", \"velit\", \"laborum\", \"anim\", \"sunt\" ], \"friends\": [ { \"id\": 0, \"name\": \"Lori Cardenas\" }, { \"id\": 1, \"name\": \"Dona Petersen\" }, { \"id\": 2, \"name\": \"Wiley Garcia\" } ], \"greeting\": \"Hello, Sadie Hicks! You have 5 unread messages.\", \"favoriteFruit\": \"banana\" }, { \"_id\": \"5fd8ae8c56c046ea55b5347e\", \"index\": 2, \"guid\": \"96067cf9-6881-4cd2-bb96-f323838056b0\", \"isActive\": false, \"balance\": \"$2,893.35\", \"picture\": \"http://placehold.it/32x32\", \"age\": 34, \"eyeColor\": \"green\", \"name\": \"Newman Foley\", \"gender\": \"male\", \"company\": \"BITENDREX\", \"email\": \"newmanfoley@bitendrex.com\", \"phone\": \"+1 (989) 436-3275\", \"address\": \"546 India Street, Nettie, Montana, 5581\", \"about\": \"Lorem laboris ut laboris ad Lorem velit reprehenderit. Eu anim quis laboris aute amet laboris est aliquip aute fugiat. Eiusmod commodo occaecat minim occaecat dolore adipisicing. Esse cupidatat aute tempor tempor officia reprehenderit occaecat ad elit duis. Esse ex sunt in voluptate ex sint sit aliquip.\\r\\n\", \"registered\": \"2015-12-05T04:23:47 -01:00\", \"latitude\": 27.765089, \"longitude\": -140.482651, \"tags\": [ \"eu\", \"proident\", \"incididunt\", \"do\", \"id\", \"cupidatat\", \"nostrud\" ], \"friends\": [ { \"id\": 0, \"name\": \"Jolene Carrillo\" }, { \"id\": 1, \"name\": \"Muriel Vaughan\" }, { \"id\": 2, \"name\": \"Gomez Tran\" } ], \"greeting\": \"Hello, Newman Foley! You have 2 unread messages.\", \"favoriteFruit\": \"strawberry\" }, { \"_id\": \"5fd8ae8c13ef14f7f6b01f60\", \"index\": 3, \"guid\": \"9fe27982-0911-49d0-b775-88ca926aa342\", \"isActive\": true, \"balance\": \"$1,824.15\", \"picture\": \"http://placehold.it/32x32\", \"age\": 32, \"eyeColor\": \"green\", \"name\": \"Olga Navarro\", \"gender\": \"female\", \"company\": \"GEEKMOSIS\", \"email\": \"olganavarro@geekmosis.com\", \"phone\": \"+1 (932) 543-2158\", \"address\": \"556 Church Lane, Hatteras, Texas, 4142\", \"about\": \"Fugiat voluptate dolore excepteur id tempor minim ullamco fugiat consectetur commodo occaecat officia sunt. Labore occaecat veniam voluptate in pariatur irure dolore exercitation elit consectetur qui tempor cupidatat. Id veniam consequat in irure quis. Id in consequat nisi exercitation nostrud do ad adipisicing nulla aliquip. Id minim veniam enim velit nisi est labore cupidatat excepteur enim exercitation et laboris.\\r\\n\", \"registered\": \"2015-03-27T04:10:23 -01:00\", \"latitude\": -57.712736, \"longitude\": -36.945847, \"tags\": [ \"ad\", \"proident\", \"excepteur\", \"est\", \"qui\", \"laboris\", \"non\" ], \"friends\": [ { \"id\": 0, \"name\": \"Tyler Hanson\" }, { \"id\": 1, \"name\": \"Owens Cummings\" }, { \"id\": 2, \"name\": \"Hewitt Rivera\" } ], \"greeting\": \"Hello, Olga Navarro! You have 1 unread messages.\", \"favoriteFruit\": \"strawberry\" }, { \"_id\": \"5fd8ae8c4a39df8215c4bd44\", \"index\": 4, \"guid\": \"3db2d592-52c3-4156-b28b-e3dc93c4e2ff\", \"isActive\": false, \"balance\": \"$1,167.53\", \"picture\": \"http://placehold.it/32x32\", \"age\": 36, \"eyeColor\": \"green\", \"name\": \"Holmes Hatfield\", \"gender\": \"male\", \"company\": \"BEDDER\", \"email\": \"holmeshatfield@bedder.com\", \"phone\": \"+1 (817) 419-3051\", \"address\": \"212 Schenectady Avenue, Hinsdale, Pennsylvania, 7761\", \"about\": \"Ullamco tempor ad ad fugiat esse incididunt amet adipisicing mollit in quis deserunt. Non esse occaecat dolore occaecat magna aute id anim aliquip est mollit tempor ex nostrud. Velit dolore consequat dolor aute labore ex minim fugiat ex eu veniam. Eiusmod aliqua ut anim do occaecat eu voluptate minim incididunt. Incididunt enim magna fugiat minim id adipisicing enim in aliqua dolor cupidatat cillum aute.\\r\\n\", \"registered\": \"2017-04-02T10:39:29 -02:00\", \"latitude\": 61.043316, \"longitude\": -73.120412, \"tags\": [ \"consequat\", \"et\", \"incididunt\", \"anim\", \"voluptate\", \"commodo\", \"anim\" ], \"friends\": [ { \"id\": 0, \"name\": \"Caroline Adkins\" }, { \"id\": 1, \"name\": \"Garrett Delgado\" }, { \"id\": 2, \"name\": \"Angeline Hewitt\" } ], \"greeting\": \"Hello, Holmes Hatfield! You have 6 unread messages.\", \"favoriteFruit\": \"apple\" }, { \"_id\": \"5fd8ae8c15a9675a901a9b85\", \"index\": 5, \"guid\": \"5cc4ea3b-f60c-4df8-9fe9-04a1e11a891f\", \"isActive\": false, \"balance\": \"$1,916.94\", \"picture\": \"http://placehold.it/32x32\", \"age\": 33, \"eyeColor\": \"green\", \"name\": \"Juliette Logan\", \"gender\": \"female\", \"company\": \"ZANITY\", \"email\": \"juliettelogan@zanity.com\", \"phone\": \"+1 (905) 499-3814\", \"address\": \"361 Preston Court, Murillo, New Hampshire, 4307\", \"about\": \"Veniam voluptate incididunt qui commodo aliqua officia enim ea. Consectetur deserunt laboris officia consequat qui excepteur incididunt tempor laboris consectetur ea est. Tempor esse aliqua irure commodo in amet. Ex anim esse occaecat aliqua ex dolor. Ea ad ut magna eu mollit eiusmod do in elit Lorem. Ex nostrud qui reprehenderit eu ea ullamco reprehenderit sint do nulla qui dolore exercitation. In magna sunt veniam aute pariatur est.\\r\\n\", \"registered\": \"2017-11-03T12:47:36 -01:00\", \"latitude\": -83.722025, \"longitude\": 89.826288, \"tags\": [ \"proident\", \"anim\", \"labore\", \"dolore\", \"ex\", \"dolor\", \"nulla\" ], \"friends\": [ { \"id\": 0, \"name\": \"Claudine Jarvis\" }, { \"id\": 1, \"name\": \"Della Freeman\" }, { \"id\": 2, \"name\": \"Suzette Gordon\" } ], \"greeting\": \"Hello, Juliette Logan! You have 10 unread messages.\", \"favoriteFruit\": \"banana\" }, { \"_id\": \"5fd8ae8c0c3da8a113345769\", \"index\": 6, \"guid\": \"d2e897fd-b0c3-4f04-9cf5-4d38e86ac1e9\", \"isActive\": true, \"balance\": \"$3,061.42\", \"picture\": \"http://placehold.it/32x32\", \"age\": 23, \"eyeColor\": \"green\", \"name\": \"Fitzpatrick Newton\", \"gender\": \"male\", \"company\": \"CINCYR\", \"email\": \"fitzpatricknewton@cincyr.com\", \"phone\": \"+1 (916) 449-2088\", \"address\": \"918 Hopkins Street, Bradenville, Hawaii, 5480\", \"about\": \"Ex cillum eiusmod non enim cupidatat ullamco nisi nulla do sit quis. Nostrud in quis incididunt elit culpa ex id aliqua elit. Fugiat ad ipsum irure sunt. Magna esse nostrud ullamco et mollit in Lorem. Qui irure excepteur minim dolore. Sit eiusmod eu magna anim nisi dolor dolore officia do.\\r\\n\", \"registered\": \"2017-02-01T08:26:09 -01:00\", \"latitude\": 1.33198, \"longitude\": 141.150845, \"tags\": [ \"commodo\", \"irure\", \"ex\", \"sunt\", \"in\", \"do\", \"cupidatat\" ], \"friends\": [ { \"id\": 0, \"name\": \"Herrera York\" }, { \"id\": 1, \"name\": \"Green Cortez\" }, { \"id\": 2, \"name\": \"Liliana Faulkner\" } ], \"greeting\": \"Hello, Fitzpatrick Newton! You have 3 unread messages.\", \"favoriteFruit\": \"banana\" } ]";
        String complexInvalidJSON = "{ \"gender\": \"female\", \"company\": \"ELENTRIX\", \"email\": \"rebarasmussen@elentrix.com\", \"phone\": \"+1 (951) 414-3811\", \"address\": \"657 Vermont Street, Gracey, West Virginia, 7792\", \"about\": \"Proident enim ex sit esse qui aliqua occaecat ex non. Mollit anim reprehenderit irure nulla occaecat voluptate magna. Duis dolor elit ipsum nostrud et deserunt exercitation mollit deserunt amet quis aliqua. Deserunt ad ea est cupidatat aliqua excepteur consectetur aliqua.\\r\\n\", \"registered\": \"2016-06-19T03:43:20 -02:00\", \"latitude\": 6.181072, \"longitude\": -17.563421, \"tags\": [ \"minim\", \"dolore\", \"pariatur\", \"non\", \"et\", \"irure\", \"reprehenderit\" ], \"friends\": [ { \"id\": 0, \"name\": \"Webster Porter\" }, { \"id\": 1, \"name\": \"Chandra Carroll\" }, { \"id\": 2, \"name\": \"Katheryn Joseph\" } ], \"greeting\": \"Hello, Reba Rasmussen! You have 3 unread messages.\", \"favoriteFruit\": \"banana\" }, { \"_id\": \"5fd8b633ada17f542d9fd794\", \"index\": 1, \"guid\": \"b3ec30cf-7a7b-4128-a351-b28c35e9a222\", \"isActive\": false, \"balance\": \"$2,790.22\", \"picture\": \"http://placehold.it/32x32\", \"age\": 34, \"eyeColor\": \"brown\", \"name\": \"Bennett Hurley\", \"gender\": \"male\", \"company\": \"EXTRAGENE\", \"email\": \"bennetthurley@extragene.com\", \"phone\": \"+1 (808) 595-2276\", \"address\": \"312 Schenck Avenue, Wilsonia, Alabama, 2735\", \"about\": \"Aliquip enim elit esse amet cillum duis irure mollit ad eu nisi quis minim. Excepteur laborum Lorem non ex. Cupidatat sit amet ad sit est aute sint nulla cillum elit aute. Ex anim cupidatat incididunt sit sit ullamco labore laboris reprehenderit dolore reprehenderit sit. Esse laboris veniam eu ut ullamco eu nostrud.\\r\\n\", \"registered\": \"2017-01-20T05:58:25 -01:00\", \"latitude\": 28.286176, \"longitude\": 118.981121, \"tags\": [ \"culpa\", \"irure\", \"deserunt\", \"ullamco\", \"quis\", \"elit\", \"in\" ], \"friends\": [ { \"id\": 0, \"name\": \"Holden Brock\" }, { \"id\": 1, \"name\": \"Phelps Hoffman\" }, { \"id\": 2, \"name\": \"Valenzuela Castillo\" } ], \"greeting\": \"Hello, Bennett Hurley! You have 8 unread messages.\", \"favoriteFruit\": \"strawberry\" }, { \"_id\": \"5fd8b633155b00a4eb572d33\", \"index\": 2, \"guid\": \"54cc8084-35a3-4d00-9b9c-97241e098e23\", \"isActive\": false, \"balance\": \"$3,684.60\", \"picture\": \"http://placehold.it/32x32\", \"age\": 39, \"eyeColor\": \"green\", \"name\": \"Cummings Henry\", \"gender\": \"male\", \"company\": \"RODEMCO\", \"email\": \"cummingshenry@rodemco.com\", \"phone\": \"+1 (906) 516-3575\", \"address\": \"891 Thomas Street, Disautel, Maine, 2680\", \"about\": \"Aliquip pariatur cupidatat aliqua dolor duis cupidatat veniam sit enim aliquip enim velit pariatur incididunt. Labore fugiat id occaecat ad eu ea. Excepteur nisi ex esse occaecat incididunt cupidatat mollit ipsum velit elit sit exercitation. Ipsum do ad ad ea id veniam ullamco quis proident Lorem amet eiusmod tempor.\\r\\n\", \"registered\": \"2017-09-05T06:23:25 -02:00\", \"latitude\": -72.198663, \"longitude\": -6.247492, \"tags\": [ \"esse\", \"veniam\", \"id\", \"velit\", \"laboris\", \"consectetur\", \"consectetur\" ], \"friends\": [ { \"id\": 0, \"name\": \"Enid Mccray\" }, { \"id\": 1, \"name\": \"Ewing Thornton\" }, { \"id\": 2, \"name\": \"Leila Bright\" } ], \"greeting\": \"Hello, Cummings Henry! You have 3 unread messages.\", \"favoriteFruit\": \"banana\" }, { \"_id\": \"5fd8b633bc9355c05eced9aa\", \"index\": 3, \"guid\": \"391ee42f-e12c-4b4f-b4b1-0126cca6d197\", \"isActive\": false, \"balance\": \"$2,647.34\", \"picture\": \"http://placehold.it/32x32\", \"age\": 39, \"eyeColor\": \"green\", \"name\": \"Matthews Collier\", \"gender\": \"male\", \"company\": \"AQUACINE\", \"email\": \"matthewscollier@aquacine.com\", \"phone\": \"+1 (967) 457-2568\", \"address\": \"509 Grove Street, Boyd, Texas, 1249\", \"about\": \"Occaecat aute commodo esse qui consequat id fugiat. Commodo amet enim pariatur deserunt sunt Lorem deserunt aliqua laborum sunt eiusmod ea minim consequat. Fugiat et dolor nulla amet dolore laboris ea irure. In velit laboris mollit dolor elit. Incididunt laboris ad deserunt enim mollit tempor eiusmod.\\r\\n\", \"registered\": \"2014-04-16T04:33:51 -02:00\", \"latitude\": -31.02391, \"longitude\": -82.501098, \"tags\": [ \"elit\", \"consequat\", \"et\", \"nisi\", \"voluptate\", \"labore\", \"cillum\" ], \"friends\": [ { \"id\": 0, \"name\": \"Alexis Chaney\" }, { \"id\": 1, \"name\": \"Winnie Garner\" }, { \"id\": 2, \"name\": \"Alison Sanders\" } ], \"greeting\": \"Hello, Matthews Collier! You have 5 unread messages.\", \"favoriteFruit\": \"banana\" }, { \"_id\": \"5fd8b63387d519c690f739f5\", \"index\": 4, \"guid\": \"24eb9900-1d7b-46c4-8f38-8fe7c8474243\", \"isActive\": true, \"balance\": \"$2,497.28\", \"picture\": \"http://placehold.it/32x32\", \"age\": 21, \"eyeColor\": \"brown\", \"name\": \"Nancy Moore\", \"gender\": \"female\", \"company\": \"PARLEYNET\", \"email\": \"nancymoore@parleynet.com\", \"phone\": \"+1 (992) 543-3018\", \"address\": \"791 Adams Street, Montura, American Samoa, 1323\", \"about\": \"Lorem pariatur consectetur excepteur duis anim id. Nulla cillum nostrud veniam occaecat aliquip cillum irure qui nulla aliquip enim sit deserunt. Id proident sit et occaecat quis exercitation sint ullamco ipsum velit anim consectetur aute. Enim nisi cillum id mollit occaecat. Dolor pariatur culpa laborum officia aliqua aliqua fugiat dolore excepteur exercitation. Cillum nulla veniam esse proident nulla Lorem laboris reprehenderit cupidatat duis anim officia est. In ipsum eiusmod incididunt laboris quis reprehenderit ipsum in esse ea aute.\\r\\n\", \"registered\": \"2017-09-28T02:43:53 -02:00\", \"latitude\": 85.863421, \"longitude\": 47.850815, \"tags\": [ \"cillum\", \"Lorem\", \"sunt\", \"est\", \"esse\", \"irure\", \"nulla\" ], \"friends\": [ { \"id\": 0, \"name\": \"Dennis Doyle\" }, { \"id\": 1, \"name\": \"Dee Rocha\" }, { \"id\": 2, \"name\": \"Ruthie Barr\" } ], \"greeting\": \"Hello, Nancy Moore! You have 8 unread messages.\", \"favoriteFruit\": \"apple\" }, { \"_id\": \"5fd8b6330c368f1ef2c78945\", \"index\": 5, \"guid\": \"f2c9b306-2994-4544-b1a9-1a1a26d15cba\", \"isActive\": false, \"balance\": \"$1,016.40\", \"picture\": \"http://placehold.it/32x32\", \"age\": 37, \"eyeColor\": \"green\", \"name\": \"Sonia Mosley\", \"gender\": \"female\", \"company\": \"FITCORE\", \"email\": \"soniamosley@fitcore.com\", \"phone\": \"+1 (860) 524-2306\", \"address\": \"701 Madison Place, Clinton, Washington, 5502\", \"about\": \"Anim eiusmod duis labore nisi veniam ex magna labore nisi irure proident ex sit. Aute ipsum aliqua est excepteur commodo eu ad cupidatat. Eu ullamco occaecat ullamco id commodo tempor incididunt. Adipisicing ut nulla ea reprehenderit incididunt proident non. Officia mollit do et id mollit ipsum proident mollit sit qui magna. Magna sint quis et tempor magna id mollit quis cillum dolore exercitation eu. Sint ipsum anim reprehenderit anim irure nisi.\\r\\n\", \"registered\": \"2017-11-24T02:26:46 -01:00\", \"latitude\": 8.158501, \"longitude\": 170.433998, \"tags\": [ \"dolore\", \"eu\", \"consectetur\", \"nostrud\", \"ut\", \"nostrud\", \"do\" ], \"friends\": [ { \"id\": 0, \"name\": \"Long Park\" }, { \"id\": 1, \"name\": \"Elisabeth Payne\" }, { \"id\": 2, \"name\": \"Fay Rodriguez\" } ], \"greeting\": \"Hello, Sonia Mosley! You have 7 unread messages.\", \"favoriteFruit\": \"strawberry\" }, { \"_id\": \"5fd8b633f415da82f53515ca\", \"index\": 6, \"guid\": \"7c027a20-ca29-4f81-912c-96c35cac15fa\", \"isActive\": false, \"balance\": \"$3,239.30\", \"picture\": \"http://placehold.it/32x32\", \"age\": 27, \"eyeColor\": \"brown\", \"name\": \"Gray Oneil\", \"gender\": \"male\", \"company\": \"XUMONK\", \"email\": \"grayoneil@xumonk.com\", \"phone\": \"+1 (854) 503-2303\", \"address\": \"473 Boardwalk , Graball, New York, 6498\", \"about\": \"Nisi ut cupidatat dolor exercitation id. Excepteur ullamco anim elit aute mollit ex eu anim nostrud. Tempor labore ut ad Lorem et do nostrud. Enim eiusmod ut ullamco laboris magna amet ipsum. Consectetur exercitation veniam adipisicing laboris deserunt. Laborum amet reprehenderit id id veniam incididunt ut consequat ullamco. Veniam ex quis Lorem reprehenderit.\\r\\n\", \"registered\": \"2018-04-07T09:46:31 -02:00\", \"latitude\": 21.681494, \"longitude\": -106.661317, \"tags\": [ \"anim\", \"nulla\", \"elit\", \"ex\", \"Lorem\", \"veniam\", \"ullamco\" ], \"friends\": [ { \"id\": 0, \"name\": \"Galloway Zamora\" }, { \"id\": 1, \"name\": \"Donaldson Gregory\" }, { \"id\": 2, \"name\": \"Dean Small\" } ], \"greeting\": \"Hello, Gray Oneil! You have 4 unread messages.\", \"favoriteFruit\": \"banana\" }";

        boolean firstIsValid = JSONValidator.from(complexValidJSON).validate();
        boolean secondIsInvalid = !JSONValidator.from(complexInvalidJSON).validate();

        assertTrue(firstIsValid);
        assertTrue(secondIsInvalid);
    }

    /**
     * Tests whether fastjson can work with json read from a .json file
     *
     * [O]rdering - Checks whether the first object in the parsed array is the first object in the .json file
     * [R]eference - References a different file
     * [C]ardinality - Checks whether the parsed array contains 3 elements
     *
     */
    @Test
    public void parseJSONFromFile() {
        final String FILENAME = "json/users.json";

        try {
            String jsonFromFile = IOUtils.toString(Objects.requireNonNull(
                Thread.currentThread().getContextClassLoader().getResourceAsStream(FILENAME)
            ));

            JSONArray parsedUsers = JSON.parseArray(jsonFromFile);

            assertNotNull(parsedUsers);
            assertEquals(parsedUsers.size(), 3);

            String firstUserJSON = parsedUsers.getJSONObject(0).toJSONString();
            assertEquals(firstUserJSON, "{\"name\":\"Justin Smid\",\"id\":0}");
        } catch (IOException ex) {
            ex.printStackTrace();
            fail();
        }
    }

    /**
     * Tests whether users can edit a string in-memory and use fastjson to parse it, and get the edited values
     */
    @Test
    public void editJSONString() {
        String jsonString = "{\"id\":1,\"name\":\"Matthijs Snijders\"}";

        User parsedUser = JSON.parseObject(jsonString, User.class);
        assertEquals(parsedUser.getName(), "Matthijs Snijders");

        jsonString = jsonString.replace("Matthijs Snijders", "Justin Smid");

        parsedUser = JSON.parseObject(jsonString, User.class);
        assertEquals(parsedUser.getName(), "Justin Smid");
    }
}
